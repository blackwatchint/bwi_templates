**29MAY2022**
- Added Isla Abramia Yolandi AFB vehicle spawn locations
- Changed Anizay "Bum Villa" flagpole and vehicle spawns
- Changed Isla Abramia Yolandi Hangars staging marker offset
- Fixed Mogilevka Barns resupply fits 2 boxes

**07NOV2021**
- Change template for split medical team
- Change carrier weapons to be disabled
- Change carrier distance on Bozcaada and Djalka to be smaller


**06OCT2021**
- Fix rooftop flagpole placed on destructible building on Gallecia template
- Add helipad spawns to Al Safyrah AFB


**03MAY2021**
- Fixed Takistan typo on Staging 4 "Loy Manara AFB Hangers"
- Added Djalka template
- Added Fapovo Island template
- Added FFAA Gallaecia template
- Added G.O.S. N'ziwasogo template


**01MAR2021**
- Fixed Mk. 49 spartan on the bow, port side of the liberty is destroyed shortly after loading Panthera
- Added Uzbin Valley
- Fixed CH-53E explodes when mounted on Tanoa and Pulau USS Freedom


**25JAN2021**
- Fixed Takistan 'Landay Village' staging marker at wrong position
- Fixed Takistan FOB Jilavur resupplyLocations is wrong
- Adjusted slotting options to work with the move of EOD/Demo to squad


**19DEC2020**
- Fixed motorpool/staging error message on Malden, Isla Duala, and Panther (Winter)
- Fixed typo in Sargento AFB Hangars
- Fixed missing vehicle error message on Sahrani
- Fixed Co-Pilot being listed above Pilot in the slotting screen
- Added more staging areas to Takistan
- Removed Takistan mountains template


**08JUL2020**
- Fixed unlocked layers on Gunkizli.
- Fixed large vehicles flipping at Brauti Parking on Vinjesvingen.
- Fixed mismatched motorpool definition for Camp Fisher on Isla Abramia.
- Fixed Villa Simona flagpole position on Panthera and Panthera Winter.
- Fixed floating vehicle spawn at Hoopakka Comms on Hellanmaa.


**06MAR2020**
- Added templates for the following terrains:
  - Hellanmaa Winter
  - Kujari
  - Suursaari
  - Vinjesvingen
  - Virolahti
- Updated all templates with temporary medical roles.
- Improved resupply spawn positions on all templates.
- Increased number of resupply spawns where possible.
- Increased helipads at Chernarus Vybor Airfield.
- Fixed Leqa Ferry spawn being under the docks on Tanoa.
- Fixed AN-2 Antonov rolling when spawned in some hangars.
- Fixed rooftop helipads causing some helicopters to explode.
- Fixed missing helipads on Panthera's Lesce Garages staging.
- Remove ACRE name channels module from all templates.
- Removed templates for:
  - Aliabad Region
  - GOS Dariyah
  - Hazar Kot Valley
  - Ihantala
  - Summa


**07AUG2019**
- Fixed BLUFOR Platoon Leader spawn position on:
  - Sahrani
  - United Sahrani


**03AUG2019**
- Added templates for the following terrains:
  - Al Rayak
  - Altis
  - Bystrica
  - Dingor
  - Gunkizli
  - Isla Abramia
  - Isla Duala
  - Lingor
  - Malden
  - Panthera
  - Panthera Winter
  - Sahrani
  - Takistan Mountains
  - Tanoa
  - United Sahrani
  - Zargabad
- Updated all templates to include boats in a dedicated layer.
- Updated all templates to include objects in a dedicated layer.
- Updated all templates with an increase forecast time of 2.5 hours.
- Updated USS Liberty artillery cannon to disable VCOM FFE.
- Updated air defence distribution on USS Liberty.
- Removed datalink from air defences on USS Freedom and USS Liberty.


**14JUL2019**
- Added templates for the following terrains:
  - Bozcaada
  - Takistan


**02MAY2019**
- Added templates for the following terrains:
  - Chernarus
  - Chernarus Summer
  - Chernarus Winter
  - Stratis


**26APR2019**
- Added templates for the following terrains:
  - Aliabad
  - Anizay
  - Dariyah
  - Fallujah
  - Hazar-Kot
  - Hellanmaa
  - Ihantala
  - Pulau
  - Rosche
  - Ruha
  - Summa
  - Summa Winter
- Removed all old templates.


**02AUG2018**
- Updated all templates with new Respawn Type module.
- Updated all templates with new `bwi_respawnBehaviour` ruleset.
- Removed `bwi_extendRespawnTimer` from all templates.


**14MAR2018**
- Added rank designations to key leadership roles.
- Added CBA Settings to all templates.
- Removed ACE Modules from all templates.


**30NOV2017**
- Updated all templates with new ACE Weather module settings.
- Updated all templates to delete the ACE BFT module.
- Removed templates for deleted terrains:
  - Desert Island
  - Diyala
  - Everon
  - Kolgujev
  - Malden
  - Nogova


**31JUL2017**
- Added template for new terrain:
  - Prei Khmaoch Luong


**26JUN2017**
- Added templates for new terrains:
  - Leskovets
  - Malden 2035
  - N'Djenahoud
  - Song Bin Tanh
- Added USS Freedom to remaining compatible templates:
  - Desert Island
  - Everon
  - Kolgujev
  - Malden
  - Malden 2035
  - Nogova
  - Porto
  - Rahmadi
- Added third headless client slot to all templates.
- Set enableDebugConsole flag on all templates.


**07JUN2017**
- Renamed "RESERVED" radio channel to "AIRNET".


**19MAY2017**
- Added README documentation on using the aircraft carrier.
- Added USS Freedom aircraft carrier to more templates:
  - Bozcaada
  - Chernarus
  - Chernarus Summer
  - Chernarus Winter
  - Porquerolles
  - Sahrani
  - Southern Sahrani
  - United Sahrani
  - Utes
  - VT5
- Moved Joint Training Center to custom project to reduce download size.


**18MAY2017**
- Added Joint Training Center.
- Added USS Freedom aircraft carrier to templates:
  - Al-Rayak
  - Altis
  - Dingor
  - Imrali Island
  - Imrali island Spring
  - Isla Abramia
  - Isla Duala
  - Island Panthera
  - Island Panthera Winter
  - Lingor
  - Stratis
  - Tanoa
  - Wake Island
- Removed Bootcamp.


**17APR2017**
- Added exception for BAF Land Rover's "MILAN" missiles.
- Added "Quick Respawn" module and synced it to the Armory crate.
- Enabled the `bwi_deploymentCleanup` respawn module on all templates.
- Removed STUI "No Squad Bar" config (now configured by CBA settings).


**17MAR2017**
- Fixed missing "player" unit in all templates.


**05MAR2017**
- Removed Kunduz template.


**28FEB2017**
- Updated all templates for new Platoon Structure.
- Updated all slot IDs in accordance with new role IDs in BWI Armory.
- Enabled `bwi_notifyOnRespawn` and `bwi_reloadLoadoutOnRespawn` respawn modules.
- Placed ACRE Channel Names module.
- Placed BWI Armory Language Names module.
- Placed BWI Utils Respawn Notice Subscribers modules.
- Configured backup ACRE radio settings.
- Added templates for new terrains:
  - Dingor
  - Kunduz
  - Panthera Winter
- Removed templates for unused terrains:
  - Angel Island
  - Helvantis
  - Kerama Islands


**27OCT2016**
- Fixed non-assignment of medical callsigns due to changes in BWI Addons.


**14OCT2016**
- Added templates for new terrains:
  - Gunkizli
  - Wake Island
  - Chernarus Winter
  - Angel Island
  - Al Rayak
  - Imrali Islands
  - Valtatie 5
  - Porquerolles
  - Kerama Islands
  - Diyala
  - Helvantis
  - Bozcaada Island
  - Island Panthera
  - Isla Duala
  - Isla Abramia
  - Lingor
- Added 3CB BAF vehicle ammo to Vehicle Clearance module exceptions.
- Fixed incorrect default callsigns for pilot slots.
- Fixed convoy hold image on Bootcamp template.
- Removed ACE Map Gesture Group Settings module from all templates.


**06SEP2016**
- Fixed incorrect role description for "Charlie" Fireteam Leader.
- Fixed invisible deployment pole on Takistan.
- Removed facewear crate due to unpredictable emptying via EDEN.
- Removed test player from Bootcamp.


**19AUG2016**
- All templates remade from scratch for Arma 3 v1.62.