## Introduction
These templates are for use by members of [Black Watch International](http://blackwatch-int.com) for making [Arma 3](https://arma3.com/) operations. The latest version of the [BWI Modpack](https://gitlab.com/blackwatchint/bwi_modpack) is a requirement for using these templates. A detailed guide covering the specifics of making missions with the Black Watch International Modpack, Addons and Templates can be found in [the wiki](https://gitlab.com/blackwatchint/bwi_templates/wikis/home).

## Reporting Issues
Any issues with the templates should be reported on our [issue tracker](https://gitlab.com/blackwatchint/bwi_templates/issues). Please make sure to include the following information:
- Description of the issue.
- Steps to reproduce.
- Where the issue occurred (singleplayer/local multiplayer/dedicated server).
- The template or mission that the issue occurred on.
- Link to the RPT file if a crash or script error occurred. [How do I find my RPT file?](https://community.bistudio.com/wiki/Crash_Files#Arma_3)

## License
These templates are licensed under the [Arma Public License Share-Alike](https://www.bistudio.com/community/licenses/arma-public-license-share-alike).

<a rel="license" href="https://www.bistudio.com/community/licenses/arma-public-license-share-alike" target="_blank" ><img src="https://www.bistudio.com/assets/img/licenses/APL-SA.png" width="100"></a>