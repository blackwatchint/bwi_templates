author = "Black Watch International";
OnLoadName = "Altis";
OnLoadMission = "Build: 07NOV2021";
enableDebugConsole = 0;

/**
 * CfgStagingAreas
 *   teleporters[] = Spawn teleporters (west, east, indep). If only 1 specified it applies for all.
 *
 * Staging
 *   name = Name of the staging area.
 *   position = Position of marker in 3DEN.
 *   teleporter = Staging area game logic object.
 *
 *   motorpoolLocations[] = Game logic objects denoting motorpool spawn locations.
 *   motorpoolLabels[] = User-friendly labels to match with the above locations.
 *   motorpoolTypes[] = Types of vehicles allowed at the above locations, as follows:
 *				1) Ground (Vehicles)
 *				2) Helipad (Helicopters)
 *				3) Hangar (Jet CAS)
 *				4) Apron (Air Transport)
 *				5) Carrier (Carrier Jet CAS)
 *			 	6) Sea (Boats)
 * 
 * 	resupplyLocations[] = Game logic objects denoting resupply spawn locations.
 */
class CfgStagingAreas
{
	teleporters[] = {"spawn_flagpole"};

	class Staging_1
	{
		name = "USS Freedom";
		position[] = {19622,26285,209.498};

		teleporter = "staging_1_flagpole";

		motorpoolLocations[] = {
			"staging_1_motorpool_1", 
			"staging_1_motorpool_2", 
			"staging_1_motorpool_3", 
			"staging_1_motorpool_4"
		};
		motorpoolLabels[] = {
			"Freedom Deck 1", 
			"Starboard Elevator 1",
			"Starboard Elevator 2",
			"Port Elevator 1",
		};
		motorpoolTypes[] = {1,5,5,2};

		resupplyLocations[] = {
			"staging_1_resupply_1",
			"staging_1_resupply_2",
			"staging_1_resupply_3"
		};
	};

	class Staging_2
	{
		name = "USS Liberty";
		position[] = {19162.3,25774.7,194.771};

		teleporter = "staging_2_flagpole";

		motorpoolLocations[] = {
			"staging_2_motorpool_1",
			"staging_2_motorpool_2",
			"staging_2_motorpool_3",
			"staging_2_motorpool_4",
			"staging_2_motorpool_5"
		};
		motorpoolLabels[] = {
			"Liberty Helipad 1",
			"Starboard Ladder 1",
			"Starboard Ladder 2",
			"Port Ladder 1",
			"Port Ladder 2"
		};
		motorpoolTypes[] = {2,6,6,6,6};

		resupplyLocations[] = {
			"staging_2_resupply_1",
			"staging_2_resupply_2",
			"staging_2_resupply_3",
			"staging_2_resupply_4"
		};
	};

	class Staging_3
	{
		name = "Airbase Terminal";
		position[] = {14630.3,16779,0};

		teleporter = "staging_3_flagpole";

		motorpoolLocations[] = {
			"staging_3_motorpool_1",
			"staging_3_motorpool_2",
			"staging_3_motorpool_3",
			"staging_3_motorpool_4",
			"staging_3_motorpool_5"
		};
		motorpoolLabels[] = {
			"Terminal Parking 1",
			"Terminal Parking 2",
			"Terminal Parking 3",
			"Terminal Parking 4",
			"Terminal Apron 1"
		};
		motorpoolTypes[] = {1,1,1,1,4};

		resupplyLocations[] = {
			"staging_3_resupply_1",
			"staging_3_resupply_2",
			"staging_3_resupply_3",
			"staging_3_resupply_4"
		};
	};

	class Staging_4
	{
		name = "Airbase Hangars";
		position[] = {14461.6,16314.7,0};

		teleporter = "staging_4_flagpole";

		motorpoolLocations[] = {
			"staging_4_motorpool_1",
			"staging_4_motorpool_2",
			"staging_4_motorpool_3"
		};
		motorpoolLabels[] = {
			"Airbase Hangar 1",
			"Airbase Hangar 2",
			"Airbase Hangar 3"
		};
		motorpoolTypes[] = {3,3,3};

		resupplyLocations[] = {
		};
	};

	class Staging_5
	{
		name = "Airbase Helipads";
		position[] = {15123.4,17284,0};

		teleporter = "staging_5_flagpole";

		motorpoolLocations[] = {
			"staging_5_motorpool_1",
			"staging_5_motorpool_2",
			"staging_5_motorpool_3",
			"staging_5_motorpool_4"
		};
		motorpoolLabels[] = {
			"Airbase Helipad 1",
			"Airbase Helipad 2",
			"Airbase Parking 1",
			"Airbase Parking 2"
		};
		motorpoolTypes[] = {2,2,1,1};

		resupplyLocations[] = {	
			"staging_5_resupply_1",
			"staging_5_resupply_2",
			"staging_5_resupply_3",
			"staging_5_resupply_4"
		};
	};

	class Staging_6
	{
		name = "AAC Airfield";
		position[] = {11616.6,12022.4,0};

		teleporter = "staging_6_flagpole";

		motorpoolLocations[] = {
			"staging_6_motorpool_1",
			"staging_6_motorpool_2",
			"staging_6_motorpool_3",
			"staging_6_motorpool_4",
			"staging_6_motorpool_5",
			"staging_6_motorpool_6",
			"staging_6_motorpool_7"
		};
		motorpoolLabels[] = {
			"AAC Parking 1",
			"AAC Parking 2",
			"AAC Parking 3",
			"AAC Parking 4",
			"AAC Helipad 1",
			"AAC Helipad 2",
			"AAC Hangar 1"
		};
		motorpoolTypes[] = {1,1,1,1,2,2,3};

		resupplyLocations[] = {
			"staging_6_resupply_1",
			"staging_6_resupply_2",
			"staging_6_resupply_3",
			"staging_6_resupply_4"
		};
	};

	class Staging_7
	{
		name = "Paros Scrapyard";
		position[] = {21079.5,16898.5,0};

		teleporter = "staging_7_flagpole";

		motorpoolLocations[] = {
			"staging_7_motorpool_1",
			"staging_7_motorpool_2",
			"staging_7_motorpool_3",
			"staging_7_motorpool_4"
		};
		motorpoolLabels[] = {
			"Paros Parking 1",
			"Paros Parking 2",
			"Paros Parking 3",
			"Paros Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_7_resupply_1",
			"staging_7_resupply_2",
			"staging_7_resupply_3",
			"staging_7_resupply_4"
		};
	};

	class Staging_8
	{
		name = "Molos Airfield";
		position[] = {26721.2,24595.7,0};

		teleporter = "staging_8_flagpole";

		motorpoolLocations[] = {
			"staging_8_motorpool_1",
			"staging_8_motorpool_2",
			"staging_8_motorpool_3",
			"staging_8_motorpool_4",
			"staging_8_motorpool_5",
			"staging_8_motorpool_6",
			"staging_8_motorpool_7"
		};
		motorpoolLabels[] = {
			"Molos Parking 1",
			"Molos Parking 2",
			"Molos Parking 3",
			"Molos Parking 4",
			"Molos Helipad 1",
			"Molos Helipad 2",
			"Molos Hangar 1"
		};
		motorpoolTypes[] = {1,1,1,1,2,2,3};

		resupplyLocations[] = {
			"staging_8_resupply_1",
			"staging_8_resupply_2",
			"staging_8_resupply_3",
			"staging_8_resupply_4"
		};
	};

	class Staging_9
	{
		name = "Magos Stadium";
		position[] = {5469.75,14907.6,0};

		teleporter = "staging_9_flagpole";

		motorpoolLocations[] = {
			"staging_9_motorpool_1",
			"staging_9_motorpool_2",
			"staging_9_motorpool_3",
			"staging_9_motorpool_4",
			"staging_9_motorpool_5",
			"staging_9_motorpool_6"
		};
		motorpoolLabels[] = {
			"Stadium Parking 1",
			"Stadium Parking 2",
			"Stadium Parking 3",
			"Stadium Parking 4",
			"Stadium Helipad 1",
			"Stadium Helipad 2"
		};
		motorpoolTypes[] = {1,1,1,1,2,2};

		resupplyLocations[] = {
			"staging_9_resupply_1",
			"staging_9_resupply_2",
			"staging_9_resupply_3",
			"staging_9_resupply_4"
		};
	};
		class Staging_10
	{
		name = "Kyra Airstrip";
		position[] = {9144.75,21627.4,0};

		teleporter = "staging_10_flagpole";

		motorpoolLocations[] = {
			"staging_10_motorpool_1",
			"staging_10_motorpool_2",
			"staging_10_motorpool_3",
			"staging_10_motorpool_4",
			"staging_10_motorpool_5",
			"staging_10_motorpool_6"
		};
		motorpoolLabels[] = {
			"Kyra Parking 1",
			"Kyra Parking 2",
			"Kyra Parking 3",
			"Kyra Parking 4",
			"Kyra Helipad 1",
			"Kyra Hangar 1"
		};
		motorpoolTypes[] = {1,1,1,1,2,3};

		resupplyLocations[] = {
			"staging_10_resupply_1",
			"staging_10_resupply_2",
			"staging_10_resupply_3",
			"staging_10_resupply_4"
		};
	};
	
	class Staging_11
	{
		name = "FOB Pyrgos";
		position[] = {17466.9,13184.9,0};

		teleporter = "staging_11_flagpole";

		motorpoolLocations[] = {
			"staging_11_motorpool_1",
			"staging_11_motorpool_2",
			"staging_11_motorpool_3",
			"staging_11_motorpool_4",
			"staging_11_motorpool_5"
		};
		motorpoolLabels[] = {
			"Pyrgos Parking 1",
			"Pyrgos Parking 2",
			"Pyrgos Parking 3",
			"Pyrgos Parking 4",
			"Pyrgos Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_11_resupply_1",
			"staging_11_resupply_2",
			"staging_11_resupply_3",
			"staging_11_resupply_4"
		};
	};
	
	class Staging_12
	{
		name = "Telos Docks";
		position[] = {15358.3,15874.8,0};

		teleporter = "staging_12_flagpole";

		motorpoolLocations[] = {
			"staging_12_motorpool_1",
			"staging_12_motorpool_2",
			"staging_12_motorpool_3",
			"staging_12_motorpool_4",
			"staging_12_motorpool_5",
			"staging_12_motorpool_6",
			"staging_12_motorpool_7"
		};
		motorpoolLabels[] = {
			"Telos Parking 1",
			"Telos Parking 2",
			"Telos Parking 3",
			"Telos Parking 4",
			"Telos Pier 1",
			"Telos Pier 2",
			"Telos Pier 3"
		};
		motorpoolTypes[] = {1,1,1,1,6,6,6};

		resupplyLocations[] = {
			"staging_12_resupply_1",
			"staging_12_resupply_2",
			"staging_12_resupply_3",
			"staging_12_resupply_4"
		};
	};
	
	class Staging_13
	{
		name = "Kavala Hospital";
		position[] = {3753.69,13013.6,0};

		teleporter = "staging_13_flagpole";

		motorpoolLocations[] = {
			"staging_13_motorpool_1",
			"staging_13_motorpool_2",
			"staging_13_motorpool_3",
			"staging_13_motorpool_4",
			"staging_13_motorpool_5"
		};
		motorpoolLabels[] = {
			"Hospital Parking 1",
			"Hospital Parking 2",
			"Hospital Parking 3",
			"Hospital Parking 4",
			"Hospital Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_13_resupply_1",
			"staging_13_resupply_2",
			"staging_13_resupply_3",
			"staging_13_resupply_4"
		};
	};
	
	class Staging_14
	{
		name = "Oreokastro Garbage Dump";
		position[] = {5878.07,20112.9,0};

		teleporter = "staging_14_flagpole";

		motorpoolLocations[] = {
			"staging_14_motorpool_1",
			"staging_14_motorpool_2",
			"staging_14_motorpool_3",
			"staging_14_motorpool_4",
			"staging_14_motorpool_5"
		};
		motorpoolLabels[] = {
			"Dump Parking 1",
			"Dump Parking 2",
			"Dump Parking 3",
			"Dump Parking 4",
			"Dump Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_14_resupply_1",
			"staging_14_resupply_2",
			"staging_14_resupply_3",
			"staging_14_resupply_4"
		};
	};
	
	class Staging_15
	{
		name = "Selakano Airfield";
		position[] = {20774.1,7263.99,0};

		teleporter = "staging_15_flagpole";

		motorpoolLocations[] = {
			"staging_15_motorpool_1",
			"staging_15_motorpool_2",
			"staging_15_motorpool_3",
			"staging_15_motorpool_4",
			"staging_15_motorpool_5",
			"staging_15_motorpool_6",
			"staging_15_motorpool_7"
		};
		motorpoolLabels[] = {
			"Selakano Parking 1",
			"Selakano Parking 2",
			"Selakano Parking 3",
			"Selakano Parking 4",
			"Selakano Helipad 1",
			"Selakano Hangar 1",
			"Selakano Apron 1"
		};
		motorpoolTypes[] = {1,1,1,1,2,3,4};

		resupplyLocations[] = {
			"staging_15_resupply_1",
			"staging_15_resupply_2",
			"staging_15_resupply_3",
			"staging_15_resupply_4"
		};
	};
	
	
	class Staging_16
	{
		name = "FOB Galati";
		position[] = {9930.66,19432.1,0};

		teleporter = "staging_16_flagpole";

		motorpoolLocations[] = {
			"staging_16_motorpool_1",
			"staging_16_motorpool_2",
			"staging_16_motorpool_3",
			"staging_16_motorpool_4",
			"staging_16_motorpool_5"
		};
		motorpoolLabels[] = {
			"Galati Parking 1",
			"Galati Parking 2",
			"Galati Parking 3",
			"Galati Parking 4",
			"Galati Helipad 1",
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_16_resupply_1",
			"staging_16_resupply_2",
			"staging_16_resupply_3",
			"staging_16_resupply_4"
		};
	};
	
	class Staging_17
	{
		name = "Chalkeia Gas Station";
		position[] = {19950.3,11446.1,0};

		teleporter = "staging_17_flagpole";

		motorpoolLocations[] = {
			"staging_17_motorpool_1",
			"staging_17_motorpool_2",
			"staging_17_motorpool_3",
			"staging_17_motorpool_4"
		};
		motorpoolLabels[] = {
			"Chalkeia Parking 1",
			"Chalkeia Parking 2",
			"Chalkeia Parking 3",
			"Chalkeia Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_17_resupply_1",
			"staging_17_resupply_2",
			"staging_17_resupply_3",
			"staging_17_resupply_4"
		};
	};
	
	class Staging_18
	{
		name = "FOB Sofia";
		position[] = {25315,21810.2,0};

		teleporter = "staging_18_flagpole";

		motorpoolLocations[] = {
			"staging_18_motorpool_1",
			"staging_18_motorpool_2",
			"staging_18_motorpool_3",
			"staging_18_motorpool_4",
			"staging_18_motorpool_5"
		};
		motorpoolLabels[] = {
			"Sofia Parking 1",
			"Sofia Parking 2",
			"Sofia Parking 3",
			"Sofia Parking 4",
			"Sofia Helipad 1",
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_18_resupply_1",
			"staging_18_resupply_2",
			"staging_18_resupply_3",
			"staging_18_resupply_4"
		};
	};
	
	class Staging_19
	{
		name = "FOB Frini";
		position[] = {14180,21231,0};

		teleporter = "staging_19_flagpole";

		motorpoolLocations[] = {
			"staging_19_motorpool_1",
			"staging_19_motorpool_2",
			"staging_19_motorpool_3",
			"staging_19_motorpool_4",
			"staging_19_motorpool_5"
		};
		motorpoolLabels[] = {
			"Frini Parking 1",
			"Frini Parking 2",
			"Frini Parking 3",
			"Frini Parking 4",
			"Frini Helipad 1",
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_19_resupply_1",
			"staging_19_resupply_2",
			"staging_19_resupply_3",
			"staging_19_resupply_4"
		};
	};
	
	class Staging_20
	{
		name = "Agios Dionysios Factory";
		position[] = {9552.11,15281.6,0};

		teleporter = "staging_20_flagpole";

		motorpoolLocations[] = {
			"staging_20_motorpool_1",
			"staging_20_motorpool_2",
			"staging_20_motorpool_3",
			"staging_20_motorpool_4"
		};
		motorpoolLabels[] = {
			"Factory Parking 1",
			"Factory Parking 2",
			"Factory Parking 3",
			"Factory Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_20_resupply_1",
			"staging_20_resupply_2",
			"staging_20_resupply_3",
			"staging_20_resupply_4"
		};
	};
	
	class Staging_21
	{
		name = "Vikos Farmstead";
		position[] = {11539.5,9450.36,0};

		teleporter = "staging_21_flagpole";

		motorpoolLocations[] = {
			"staging_21_motorpool_1",
			"staging_21_motorpool_2",
			"staging_21_motorpool_3",
			"staging_21_motorpool_4",
			"staging_21_motorpool_5"
		};
		motorpoolLabels[] = {
			"Vikos Parking 1",
			"Vikos Parking 2",
			"Vikos Parking 3",
			"Vikos Parking 4",
			"Vikos Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_21_resupply_1",
			"staging_21_resupply_2",
			"staging_21_resupply_3",
			"staging_21_resupply_4"
		};
	};
	
	class Staging_22
	{
		name = "FOB Pefkas";
		position[] = {20594.5,20105.8,0};

		teleporter = "staging_22_flagpole";

		motorpoolLocations[] = {
			"staging_22_motorpool_1",
			"staging_22_motorpool_2",
			"staging_22_motorpool_3",
			"staging_22_motorpool_4",
			"staging_22_motorpool_5"
		};
		motorpoolLabels[] = {
			"Pefkas Parking 1",
			"Pefkas Parking 2",
			"Pefkas Parking 3",
			"Pefkas Parking 4",
			"Pefkas Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_22_resupply_1",
			"staging_22_resupply_2",
			"staging_22_resupply_3",
			"staging_22_resupply_4"
		};
	};
};