author = "Black Watch International";
OnLoadName = "Fallujah";
OnLoadMission = "Build: 07NOV2021";
enableDebugConsole = 0;

/**
 * CfgStagingAreas
 *   teleporters[] = Spawn teleporters (west, east, indep). If only 1 specified it applies for all.
 *
 * Staging
 *   name = Name of the staging area.
 *   position = Position of marker in 3DEN.
 *   teleporter = Staging area game logic object.
 *
 *   motorpoolLocations[] = Game logic objects denoting motorpool spawn locations.
 *   motorpoolLabels[] = User-friendly labels to match with the above locations.
 *   motorpoolTypes[] = Types of vehicles allowed at the above locations, as follows:
 *				1) Ground (Vehicles)
 *				2) Helipad (Helicopters)
 *				3) Hangar (Jet CAS)
 *				4) Apron (Air Transport)
 *				5) Carrier (Carrier Jet CAS)
 *			 	6) Sea (Boats)
 * 
 * 	resupplyLocations[] = Game logic objects denoting resupply spawn locations.
 */
class CfgStagingAreas
{
	teleporters[] = {"spawn_flagpole"};

	class Staging_1
	{
		name = "Airport East";
		position[] = {7994.38,2474.21,0};

		teleporter = "staging_1_flagpole";

		motorpoolLocations[] = {
			"staging_1_motorpool_1", 
			"staging_1_motorpool_2", 
			"staging_1_motorpool_3", 
			"staging_1_motorpool_4", 
			"staging_1_motorpool_5", 
			"staging_1_motorpool_6", 
			"staging_1_motorpool_7", 
			"staging_1_motorpool_8"  
		};
		motorpoolLabels[] = {
			"Airport Barracks 1", 
			"Airport Barracks 2", 
			"Airport Barracks 3", 
			"Airport Barracks 4",
			"Airport Helipad 1",
			"Airport Helipad 2",
			"Airport Helipad 3",
			"Airport Helipad 4"
		};
		motorpoolTypes[] = {1,1,1,1,2,2,2,2};

		resupplyLocations[] = {
			"staging_1_resupply_1",
			"staging_1_resupply_2",
			"staging_1_resupply_3",
			"staging_1_resupply_4"
		};
	};

	class Staging_2
	{
		name = "Airport Runway";
		position[] = {8326.31,1447.61,0};

		teleporter = "staging_2_flagpole";

		motorpoolLocations[] = {
			"staging_2_motorpool_1",
			"staging_2_motorpool_2",
			"staging_2_motorpool_3",
			"staging_2_motorpool_4"
		};
		motorpoolLabels[] = {
			"Airport Hangar 1",
			"Airport Hangar 2",
			"Airport Hangar 3",
			"Airport Apron 1"
		};
		motorpoolTypes[] = {3,3,3,4};

		resupplyLocations[] = {
			"staging_2_resupply_1",
			"staging_2_resupply_2",
			"staging_2_resupply_3"
		};
	};

	class Staging_3
	{
		name = "Airport West";
		position[] = {7799.02,1834.93,0};

		teleporter = "staging_3_flagpole";

		motorpoolLocations[] = {
			"staging_3_motorpool_1",
			"staging_3_motorpool_2",
			"staging_3_motorpool_3",
			"staging_3_motorpool_4",
			"staging_3_motorpool_5",
			"staging_3_motorpool_6"
		};
		motorpoolLabels[] = {
			"Airport Barracks 1",
			"Airport Barracks 2",
			"Airport Barracks 3",
			"Airport Apron 1",
			"Airport Apron 2",
			"Airport Apron 3"
		};
		motorpoolTypes[] = {1,1,1,4,4,4};

		resupplyLocations[] = {
			"staging_3_resupply_1",
			"staging_3_resupply_2",
			"staging_3_resupply_3",
			"staging_3_resupply_4"
		};
	};

	class Staging_4
	{
		name = "FOB Norton";
		position[] = {5673.13,9879.76,0};

		teleporter = "staging_4_flagpole";

		motorpoolLocations[] = {
			"staging_4_motorpool_1",
			"staging_4_motorpool_2",
			"staging_4_motorpool_3",
			"staging_4_motorpool_4",
			"staging_4_motorpool_5",
			"staging_4_motorpool_6",
			"staging_4_motorpool_7"
		};
		motorpoolLabels[] = {
			"FOB Containers 1",
			"FOB Containers 2",
			"FOB Deer Stand 1",
			"FOB Deer Stand 2",
			"FOB Helipad 1",
			"FOB Helipad 2",
			"FOB Helipad 3"
		};
		motorpoolTypes[] = {1,1,1,1,2,2,2};

		resupplyLocations[] = {
			"staging_4_resupply_1",
			"staging_4_resupply_2",
			"staging_4_resupply_3",
			"staging_4_resupply_4"
		};
	};

	class Staging_5
	{
		name = "FOB Jolan";
		position[] = {2771.86,6348.8,0};

		teleporter = "staging_5_flagpole";

		motorpoolLocations[] = {
			"staging_5_motorpool_1",
			"staging_5_motorpool_2",
			"staging_5_motorpool_3",
			"staging_5_motorpool_4",
			"staging_5_motorpool_5",
			"staging_5_motorpool_6"
		};
		motorpoolLabels[] = {
			"Jolan Parking 1",
			"Jolan Parking 2",
			"Jolan Parking 3",
			"Jolan Parking 4",
			"Jolan Helipad 1",
			"Jolan Helipad 2"		
		};
		motorpoolTypes[] = {1,1,1,1,2,2};

		resupplyLocations[] = {
			"staging_5_resupply_1",
			"staging_5_resupply_2",
			"staging_5_resupply_3",
			"staging_5_resupply_4"
		};
	};

	class Staging_6
	{
		name = "Power Station";
		position[] = {662.933,8310.29,0};

		teleporter = "staging_6_flagpole";

		motorpoolLocations[] = {
			"staging_6_motorpool_1",
			"staging_6_motorpool_2",
			"staging_6_motorpool_3"
		};
		motorpoolLabels[] = {
			"Power Stn. Yard 1",
			"Power Stn. Yard 2",
			"Power Stn. Yard 3"
		};
		motorpoolTypes[] = {1,1,1};

		resupplyLocations[] = {
			"staging_6_resupply_1",
			"staging_6_resupply_2",
			"staging_6_resupply_3",
			"staging_6_resupply_4"
		};
	};

	class Staging_7
	{
		name = "Euphrates Hospital";
		position[] = {2459.91,4884.91,0};

		teleporter = "staging_7_flagpole";

		motorpoolLocations[] = {
			"staging_7_motorpool_1",
			"staging_7_motorpool_2",
			"staging_7_motorpool_3",
			"staging_7_motorpool_4",
			"staging_7_motorpool_5",
			"staging_7_motorpool_6"
		};
		motorpoolLabels[] = {
			"Hospital Parking 1",
			"Hospital Parking 2",
			"Hospital Parking 3",
			"Hospital Parking 4",
			"Hospital Helipad 1",
			"Hospital Helipad 2"
		};
		motorpoolTypes[] = {1,1,1,1,2,2};

		resupplyLocations[] = {
			"staging_7_resupply_1",
			"staging_7_resupply_2",
			"staging_7_resupply_3",
			"staging_7_resupply_4"
		};
	};

	class Staging_8
	{
		name = "Abandoned Yard";
		position[] = {4604.08,2381.25,0};

		teleporter = "staging_8_flagpole";

		motorpoolLocations[] = {
			"staging_8_motorpool_1",
			"staging_8_motorpool_2",
			"staging_8_motorpool_3",
			"staging_8_motorpool_4"
		};
		motorpoolLabels[] = {
			"Yard Parking 1",
			"Yard Parking 2",
			"Yard Parking 3",
			"Yard Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_8_resupply_1",
			"staging_8_resupply_2",
			"staging_8_resupply_3",
			"staging_8_resupply_4"
		};
	};

	class Staging_9
	{
		name = "Highway Hotel";
		position[] = {8256.04,5481.99,0};

		teleporter = "staging_9_flagpole";

		motorpoolLocations[] = {
			"staging_9_motorpool_1",
			"staging_9_motorpool_2",
			"staging_9_motorpool_3",
			"staging_9_motorpool_4"
		};
		motorpoolLabels[] = {
			"Hotel Parking 1",
			"Hotel Parking 2",
			"Hotel Parking 3",
			"Hotel Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_9_resupply_1",
			"staging_9_resupply_2",
			"staging_9_resupply_3",
			"staging_9_resupply_4"
		};
	};
	
	class Staging_10
	{
		name = "Fallujah Tower";
		position[] = {4686.65,5258.16,0};

		teleporter = "staging_10_flagpole";

		motorpoolLocations[] = {
			"staging_10_motorpool_1",
			"staging_10_motorpool_2",
			"staging_10_motorpool_3",
		};
		motorpoolLabels[] = {
			"Tower Yard 1",
			"Tower Yard 2",
			"Tower Yard 3",
		};
		motorpoolTypes[] = {1,1,1};

		resupplyLocations[] = {
			"staging_10_resupply_1",
			"staging_10_resupply_2",
			"staging_10_resupply_3",
			"staging_10_resupply_4"
		};
	};
};