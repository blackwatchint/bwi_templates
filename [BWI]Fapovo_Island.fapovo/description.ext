author = "Black Watch International";
OnLoadName = "Fapovo Island";
OnLoadMission = "Build: 07NOV2021";
enableDebugConsole = 0;

/**
 * CfgStagingAreas
 *   teleporters[] = Spawn teleporters (west, east, indep). If only 1 specified it applies for all.
 *
 * Staging
 *   name = Name of the staging area.
 *   position = Position of marker in 3DEN.
 *   teleporter = Staging area game logic object.
 *
 *   motorpoolLocations[] = Game logic objects denoting motorpool spawn locations.
 *   motorpoolLabels[] = User-friendly labels to match with the above locations.
 *   motorpoolTypes[] = Types of vehicles allowed at the above locations, as follows:
 *				1) Ground (Vehicles)
 *				2) Helipad (Helicopters)
 *				3) Hangar (Jet CAS)
 *				4) Apron (Air Transport)
 *				5) Carrier (Carrier Jet CAS)
 *			 	6) Sea (Boats)
 * 
 * 	resupplyLocations[] = Game logic objects denoting resupply spawn locations.
 */
class CfgStagingAreas
{
	teleporters[] = {"spawn_flagpole"};

	class Staging_1
	{
		name = "MOB Mermaid Garages";
		position[] = {454,4543,0};

		teleporter = "staging_1_flagpole";

		motorpoolLocations[] = {
			"staging_1_motorpool_1", 
			"staging_1_motorpool_2", 
			"staging_1_motorpool_3", 
			"staging_1_motorpool_4", 
			"staging_1_motorpool_5", 
			"staging_1_motorpool_6", 
			"staging_1_motorpool_7", 
			"staging_1_motorpool_8"
		};
		motorpoolLabels[] = {
			"Mermaid Parking 1", 
			"Mermaid Parking 2", 
			"Mermaid Parking 3",
			"Mermaid Parking 4",
			"Mermaid Parking 5",
			"Mermaid Parking 6",
			"Mermaid Helipad 1",
			"Mermaid Apron 1"
		};
		motorpoolTypes[] = {1,1,1,1,1,1,2,4};

		resupplyLocations[] = {
			"staging_1_resupply_1",
			"staging_1_resupply_2",
			"staging_1_resupply_3",
			"staging_1_resupply_4"
		};
	};

	class Staging_2
	{
		name = "MOB Mermaid Hangars";
		position[] = {618,4529,0};

		teleporter = "staging_2_flagpole";

		motorpoolLocations[] = {
			"staging_2_motorpool_1"
		};
		motorpoolLabels[] = {
			"Mermaid Hangar 1"
		};
		motorpoolTypes[] = {3};

		resupplyLocations[] = {
			"staging_2_resupply_1",
			"staging_2_resupply_2",
			"staging_2_resupply_3",
			"staging_2_resupply_4"
		};
	};

	class Staging_3
	{
		name = "Ivanograd Helipads";
		position[] = {4505,453,0};

		teleporter = "staging_3_flagpole";

		motorpoolLocations[] = {
			"staging_3_motorpool_1",
			"staging_3_motorpool_2",
			"staging_3_motorpool_3",
			"staging_3_motorpool_4",
			"staging_3_motorpool_5",
			"staging_3_motorpool_6"
		};
		motorpoolLabels[] = {
			"Invanograd Parking 1",
			"Invanograd Parking 2",
			"Invanograd Parking 3",
			"Invanograd Parking 4",
			"Invanograd Helipad 1",
			"Invanograd Helipad 2"
		};
		motorpoolTypes[] = {1,1,1,1,2,2};

		resupplyLocations[] = {
			"staging_3_resupply_1",
			"staging_3_resupply_2",
			"staging_3_resupply_3",
			"staging_3_resupply_4"
		};
	};
	
	class Staging_4
	{
		name = "Ivanograd Hangars";
		position[] = {4473,396,0};

		teleporter = "staging_4_flagpole";

		motorpoolLocations[] = {
			"staging_4_motorpool_1",
			"staging_4_motorpool_2"
		};
		motorpoolLabels[] = {
			"Viveiro Hangar 1",
			"Viveiro Hangar 2"
		};
		motorpoolTypes[] = {3,3};

		resupplyLocations[] = {
			"staging_4_resupply_1",
			"staging_4_resupply_2",
			"staging_4_resupply_3",
			"staging_4_resupply_4"
		};
	};

	class Staging_5
	{
		name = "SBP HQ";
		position[] = {1296,377,0};

		teleporter = "staging_5_flagpole";

		motorpoolLocations[] = {
			"staging_5_motorpool_1",
			"staging_5_motorpool_2",
			"staging_5_motorpool_3",
			"staging_5_motorpool_4",
			"staging_5_motorpool_5"
		};
		motorpoolLabels[] = {
			"SBP Parking 1",
			"SBP Parking 2",
			"SBP Parking 3",
			"SBP Parking 4",
			"SBP Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_5_resupply_1",
			"staging_5_resupply_2",
			"staging_5_resupply_3",
			"staging_5_resupply_4"
		};
	};

	class Staging_6
	{
		name = "Zelina Office";
		position[] = {2562,3172,2.04752};

		teleporter = "staging_6_flagpole";

		motorpoolLocations[] = {
			"staging_6_motorpool_1",
			"staging_6_motorpool_2",
			"staging_6_motorpool_3",
			"staging_6_motorpool_4"
		};
		motorpoolLabels[] = {
			"Zelina Parking 1",
			"Zelina Parking 2",
			"Zelina Parking 3",
			"Zelina Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_6_resupply_1",
			"staging_6_resupply_2",
			"staging_6_resupply_3",
			"staging_6_resupply_4"
		};
	};

	class Staging_7
	{
		name = "Lixovo Army Ammo Dump";
		position[] = {4086,2276,0};

		teleporter = "staging_7_flagpole";

		motorpoolLocations[] = {
			"staging_7_motorpool_1",
			"staging_7_motorpool_2",
			"staging_7_motorpool_3",
			"staging_7_motorpool_4"
		};
		motorpoolLabels[] = {
			"Lixovo Parking 1",
			"Lixovo Parking 2",
			"Lixovo Parking 3",
			"Lixovo Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_7_resupply_1",
			"staging_7_resupply_2",
			"staging_7_resupply_3",
			"staging_7_resupply_4"
		};
	};

	class Staging_8
	{
		name = "Factory Jedan";
		position[] = {3608,4167,0};

		teleporter = "staging_8_flagpole";

		motorpoolLocations[] = {
			"staging_8_motorpool_1",
			"staging_8_motorpool_2",
			"staging_8_motorpool_3",
			"staging_8_motorpool_4"
		};
		motorpoolLabels[] = {
			"Jedan Parking 1",
			"Jedan Parking 2",
			"Jedan Parking 3",
			"Jedan Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_8_resupply_1",
			"staging_8_resupply_2",
			"staging_8_resupply_3",
			"staging_8_resupply_4"
		};
	};
	
	class Staging_9
	{
		name = "Border Crossing North";
		position[] = {4333,5876,0};

		teleporter = "staging_9_flagpole";

		motorpoolLocations[] = {
			"staging_9_motorpool_1",
			"staging_9_motorpool_2",
			"staging_9_motorpool_3",
			"staging_9_motorpool_4"
		};
		motorpoolLabels[] = {
			"Border Roadside 1",
			"Border Roadside 2",
			"Border Roadside 3",
			"Border Roadside 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_9_resupply_1",
			"staging_9_resupply_2",
			"staging_9_resupply_3",
			"staging_9_resupply_4"
		};
	};
	
	class Staging_10
	{
		name = "Kruger Factory Yard";
		position[] = {1069,7701,0};

		teleporter = "staging_10_flagpole";

		motorpoolLocations[] = {
			"staging_10_motorpool_1",
			"staging_10_motorpool_2",
			"staging_10_motorpool_3",
			"staging_10_motorpool_4"
		};
		motorpoolLabels[] = {
			"Kruger Parking 1",
			"Kruger Parking 2",
			"Kruger Parking 3",
			"Kruger Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_10_resupply_1",
			"staging_10_resupply_2",
			"staging_10_resupply_3",
			"staging_10_resupply_4"
		};
	};
	
	class Staging_11
	{
		name = "Storage Facility 71";
		position[] = {3919,7843,0};

		teleporter = "staging_11_flagpole";

		motorpoolLocations[] = {
			"staging_11_motorpool_1",
			"staging_11_motorpool_2",
			"staging_11_motorpool_3",
			"staging_11_motorpool_4"
		};
		motorpoolLabels[] = {
			"Facility Parking 1",
			"Facility Parking 2",
			"Facility Parking 3",
			"Facility Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_11_resupply_1",
			"staging_11_resupply_2",
			"staging_11_resupply_3",
			"staging_11_resupply_4"
		};
	};
	
	class Staging_12
	{
		name = "Isla Noel";
		position[] = {6275,2493,0};

		teleporter = "staging_12_flagpole";

		motorpoolLocations[] = {
			"staging_12_motorpool_1",
			"staging_12_motorpool_2",
			"staging_12_motorpool_3"
		};
		motorpoolLabels[] = {
			"Isla Noel Parking 1",
			"Isla Noel Pier 1",
			"Isla Noel Pier 2"
		};
		motorpoolTypes[] = {1,6,6};

		resupplyLocations[] = {
			"staging_12_resupply_1",
			"staging_12_resupply_2",
			"staging_12_resupply_3",
			"staging_12_resupply_4"
		};
	};
	
	class Staging_13
	{
		name = "Armybase Sharkovo";
		position[] = {8966,1789,0};

		teleporter = "staging_13_flagpole";

		motorpoolLocations[] = {
			"staging_13_motorpool_1",
			"staging_13_motorpool_2",
			"staging_13_motorpool_3",
			"staging_13_motorpool_4",
			"staging_13_motorpool_5",
			"staging_13_motorpool_6"
		};
		motorpoolLabels[] = {
			"Sharkovo Parking 1",
			"Sharkovo Parking 2",
			"Sharkovo Parking 3",
			"Sharkovo Parking 4",
			"Sharkovo Helipad 1",
			"Sharkovo Helipad 2"
		};
		motorpoolTypes[] = {1,1,1,1,2,2};

		resupplyLocations[] = {
			"staging_13_resupply_1",
			"staging_13_resupply_2",
			"staging_13_resupply_3",
			"staging_13_resupply_4"
		};
	};
	
	class Staging_14
	{
		name = "Vilar Village";
		position[] = {6913,1005,0};

		teleporter = "staging_14_flagpole";

		motorpoolLocations[] = {
			"staging_14_motorpool_1",
			"staging_14_motorpool_2",
			"staging_14_motorpool_3",
			"staging_14_motorpool_4"
		};
		motorpoolLabels[] = {
			"Vilar Parking 1",
			"Vilar Parking 2",
			"Vilar Parking 3",
			"Vilar Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_14_resupply_1",
			"staging_14_resupply_2",
			"staging_14_resupply_3",
			"staging_14_resupply_4"
		};
	};
	
	class Staging_15
	{
		name = "Botana International";
		position[] = {8348,8297,0};

		teleporter = "staging_15_flagpole";

		motorpoolLocations[] = {
			"staging_15_motorpool_1",
			"staging_15_motorpool_2",
			"staging_15_motorpool_3",
			"staging_15_motorpool_4",
			"staging_15_motorpool_5",
			"staging_15_motorpool_6"
		};
		motorpoolLabels[] = {
			"Botana Parking 1",
			"Botana Parking 2",
			"Botana Parking 3",
			"Botana Parking 4",
			"Botana Helipad 1",
			"Botana Helipad 2"
		};
		motorpoolTypes[] = {1,1,1,1,2,2};

		resupplyLocations[] = {
			"staging_15_resupply_1",
			"staging_15_resupply_2",
			"staging_15_resupply_3",
			"staging_15_resupply_4"
		};
	};
	
	class Staging_16
	{
		name = "Botana Hangars";
		position[] = {8469,8285,0};

		teleporter = "staging_16_flagpole";

		motorpoolLocations[] = {
			"staging_16_motorpool_1",
			"staging_16_motorpool_2"
		};
		motorpoolLabels[] = {
			"Botana Hangar 1",
			"Botana Hangar 2"
		};
		motorpoolTypes[] = {3,3};

		resupplyLocations[] = {
			"staging_16_resupply_1",
			"staging_16_resupply_2",
			"staging_16_resupply_3",
			"staging_16_resupply_4"
		};
	};
};