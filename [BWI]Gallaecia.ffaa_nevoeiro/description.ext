author = "Black Watch International";
OnLoadName = "Gallaecia";
OnLoadMission = "Build: 07NOV2021";
enableDebugConsole = 0;

/**
 * CfgStagingAreas
 *   teleporters[] = Spawn teleporters (west, east, indep). If only 1 specified it applies for all.
 *
 * Staging
 *   name = Name of the staging area.
 *   position = Position of marker in 3DEN.
 *   teleporter = Staging area game logic object.
 *
 *   motorpoolLocations[] = Game logic objects denoting motorpool spawn locations.
 *   motorpoolLabels[] = User-friendly labels to match with the above locations.
 *   motorpoolTypes[] = Types of vehicles allowed at the above locations, as follows:
 *				1) Ground (Vehicles)
 *				2) Helipad (Helicopters)
 *				3) Hangar (Jet CAS)
 *				4) Apron (Air Transport)
 *				5) Carrier (Carrier Jet CAS)
 *			 	6) Sea (Boats)
 * 
 * 	resupplyLocations[] = Game logic objects denoting resupply spawn locations.
 */
class CfgStagingAreas
{
	teleporters[] = {"spawn_flagpole"};

	class Staging_1
	{
		name = "Vide Aerodromo";
		position[] = {8129.98,1702,0};

		teleporter = "staging_1_flagpole";

		motorpoolLocations[] = {
			"staging_1_motorpool_1", 
			"staging_1_motorpool_2", 
			"staging_1_motorpool_3", 
			"staging_1_motorpool_4", 
			"staging_1_motorpool_5", 
			"staging_1_motorpool_6"
		};
		motorpoolLabels[] = {
			"Aerodromo Parking 1", 
			"Aerodromo Parking 2", 
			"Aerodromo Parking 3",
			"Aerodromo Parking 4",
			"Aerodromo Helipad 1",
			"Aerodromo Hangar 1"
		};
		motorpoolTypes[] = {1,1,1,1,2,3};

		resupplyLocations[] = {
			"staging_1_resupply_1",
			"staging_1_resupply_2",
			"staging_1_resupply_3",
			"staging_1_resupply_4"
		};
	};

	class Staging_2
	{
		name = "FOB Tabarnia";
		position[] = {6139.12,6899.97,2.84737};

		teleporter = "staging_2_flagpole";

		motorpoolLocations[] = {
			"staging_2_motorpool_1",
			"staging_2_motorpool_2",
			"staging_2_motorpool_3",
			"staging_2_motorpool_4",
			"staging_2_motorpool_5",
			"staging_2_motorpool_6"
		};
		motorpoolLabels[] = {
			"Tabarnia Parking 1",
			"Tabarnia Parking 2",
			"Tabarnia Parking 3",
			"Tabarnia Parking 4",
			"Tabarnia Helipad 1",
			"Tabarnia Helipad 2"
		};
		motorpoolTypes[] = {1,1,1,1,2,2};

		resupplyLocations[] = {
			"staging_2_resupply_1",
			"staging_2_resupply_2",
			"staging_2_resupply_3",
			"staging_2_resupply_4"
		};
	};

	class Staging_3
	{
		name = "Veigacha Industrial Park";
		position[] = {8221,8828,0};

		teleporter = "staging_3_flagpole";

		motorpoolLocations[] = {
			"staging_3_motorpool_1",
			"staging_3_motorpool_2",
			"staging_3_motorpool_3",
			"staging_3_motorpool_4"
		};
		motorpoolLabels[] = {
			"Veigacha Parking 1",
			"Veigacha Parking 2",
			"Veigacha Parking 3",
			"Veigacha Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_3_resupply_1",
			"staging_3_resupply_2",
			"staging_3_resupply_3",
			"staging_3_resupply_4"
		};
	};
	
	class Staging_4
	{
		name = "Viveiro Depot";
		position[] = {9673,6898,0};

		teleporter = "staging_4_flagpole";

		motorpoolLocations[] = {
			"staging_4_motorpool_1",
			"staging_4_motorpool_2",
			"staging_4_motorpool_3",
			"staging_4_motorpool_4",
			"staging_4_motorpool_5",
			"staging_4_motorpool_6",
			"staging_4_motorpool_7",
			"staging_4_motorpool_8",
			"staging_4_motorpool_9",
			"staging_4_motorpool_10"
		};
		motorpoolLabels[] = {
			"Viveiro Parking 1",
			"Viveiro Parking 2",
			"Viveiro Parking 3",
			"Viveiro Parking 4",
			"Viveiro Parking 5",
			"Viveiro Parking 6",
			"Viveiro Helipad 1",
			"Viveiro Helipad 2",
			"Viveiro Helipad 3",
			"Viveiro Helipad 4"
		};
		motorpoolTypes[] = {1,1,1,1,1,1,2,2,2,2};

		resupplyLocations[] = {
			"staging_4_resupply_1",
			"staging_4_resupply_2",
			"staging_4_resupply_3",
			"staging_4_resupply_4"
		};
	};

	class Staging_5
	{
		name = "Funcuberta Fields";
		position[] = {7280,4182,0};

		teleporter = "staging_5_flagpole";

		motorpoolLocations[] = {
			"staging_5_motorpool_1",
			"staging_5_motorpool_2",
			"staging_5_motorpool_3",
			"staging_5_motorpool_4"
		};
		motorpoolLabels[] = {
			"Funcuberta Parking 1",
			"Funcuberta Parking 2",
			"Funcuberta Parking 3",
			"Funcuberta Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_5_resupply_1",
			"staging_5_resupply_2",
			"staging_5_resupply_3",
			"staging_5_resupply_4"
		};
	};

	class Staging_6
	{
		name = "Almoite Safehouse";
		position[] = {5342,4614,0};

		teleporter = "staging_6_flagpole";

		motorpoolLocations[] = {
			"staging_6_motorpool_1",
			"staging_6_motorpool_2",
			"staging_6_motorpool_3",
			"staging_6_motorpool_4"
		};
		motorpoolLabels[] = {
			"Almoite Parking 1",
			"Almoite Parking 2",
			"Almoite Parking 3",
			"Almoite Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_6_resupply_1",
			"staging_6_resupply_2",
			"staging_6_resupply_3",
			"staging_6_resupply_4"
		};
	};

	class Staging_7
	{
		name = "Bouzas Depot";
		position[] = {4873,2310,0};

		teleporter = "staging_7_flagpole";

		motorpoolLocations[] = {
			"staging_7_motorpool_1",
			"staging_7_motorpool_2",
			"staging_7_motorpool_3",
			"staging_7_motorpool_4",
			"staging_7_motorpool_5",
			"staging_7_motorpool_6"
		};
		motorpoolLabels[] = {
			"Bouzas Parking 1",
			"Bouzas Parking 2",
			"Bouzas Parking 3",
			"Bouzas Parking 4",
			"Bouzas Parking 5",
			"Bouzas Parking 6"
		};
		motorpoolTypes[] = {1,1,1,1,1,1};

		resupplyLocations[] = {
			"staging_7_resupply_1",
			"staging_7_resupply_2",
			"staging_7_resupply_3",
			"staging_7_resupply_4"
		};
	};

	class Staging_8
	{
		name = "Vide Backlot";
		position[] = {6977,885,0};

		teleporter = "staging_8_flagpole";

		motorpoolLocations[] = {
			"staging_8_motorpool_1",
			"staging_8_motorpool_2",
			"staging_8_motorpool_3",
			"staging_8_motorpool_4"
		};
		motorpoolLabels[] = {
			"Backlot Parking 1",
			"Backlot Parking 2",
			"Backlot Parking 3",
			"Backlot Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_8_resupply_1",
			"staging_8_resupply_2",
			"staging_8_resupply_3",
			"staging_8_resupply_4"
		};
	};
	
	class Staging_9
	{
		name = "Piuca Warehouses";
		position[] = {3939,6802,0};

		teleporter = "staging_9_flagpole";

		motorpoolLocations[] = {
			"staging_9_motorpool_1",
			"staging_9_motorpool_2",
			"staging_9_motorpool_3",
			"staging_9_motorpool_4",
			"staging_9_motorpool_5",
			"staging_9_motorpool_6"
		};
		motorpoolLabels[] = {
			"Piuca Parking 1",
			"Piuca Parking 2",
			"Piuca Parking 3",
			"Piuca Parking 4",
			"Piuca Parking 5",
			"Piuca Parking 6"
		};
		motorpoolTypes[] = {1,1,1,1,1,1};

		resupplyLocations[] = {
			"staging_9_resupply_1",
			"staging_9_resupply_2",
			"staging_9_resupply_3",
			"staging_9_resupply_4"
		};
	};
	
	class Staging_10
	{
		name = "San Gines Cathedral";
		position[] = {882,8685,0};

		teleporter = "staging_10_flagpole";

		motorpoolLocations[] = {
			"staging_10_motorpool_1",
			"staging_10_motorpool_2",
			"staging_10_motorpool_3",
			"staging_10_motorpool_4"
		};
		motorpoolLabels[] = {
			"San Gines Parking 1",
			"San Gines Parking 2",
			"San Gines Parking 3",
			"San Gines Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_10_resupply_1",
			"staging_10_resupply_2",
			"staging_10_resupply_3",
			"staging_10_resupply_4"
		};
	};
	
	class Staging_11
	{
		name = "Saa Churchyard";
		position[] = {1771,7065,0};

		teleporter = "staging_11_flagpole";

		motorpoolLocations[] = {
			"staging_11_motorpool_1",
			"staging_11_motorpool_2",
			"staging_11_motorpool_3",
			"staging_11_motorpool_4"
		};
		motorpoolLabels[] = {
			"Saa Parking 1",
			"Saa Parking 2",
			"Saa Parking 3",
			"Saa Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_11_resupply_1",
			"staging_11_resupply_2",
			"staging_11_resupply_3",
			"staging_11_resupply_4"
		};
	};
	
	class Staging_12
	{
		name = "O Pazo";
		position[] = {623,6010,0};

		teleporter = "staging_12_flagpole";

		motorpoolLocations[] = {
			"staging_12_motorpool_1",
			"staging_12_motorpool_2",
			"staging_12_motorpool_3",
			"staging_12_motorpool_4"
		};
		motorpoolLabels[] = {
			"O Pazo Parking 1",
			"O Pazo Parking 2",
			"O Pazo Parking 3",
			"O Pazo Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_12_resupply_1",
			"staging_12_resupply_2",
			"staging_12_resupply_3",
			"staging_12_resupply_4"
		};
	};
	
	class Staging_13
	{
		name = "Sangunedo Roadside";
		position[] = {2963,3688,0};

		teleporter = "staging_13_flagpole";

		motorpoolLocations[] = {
			"staging_13_motorpool_1",
			"staging_13_motorpool_2",
			"staging_13_motorpool_3",
			"staging_13_motorpool_4"
		};
		motorpoolLabels[] = {
			"Sangunedo Parking 1",
			"Sangunedo Parking 2",
			"Sangunedo Parking 3",
			"Sangunedo Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_13_resupply_1",
			"staging_13_resupply_2",
			"staging_13_resupply_3",
			"staging_13_resupply_4"
		};
	};
	
	class Staging_14
	{
		name = "Vilar Village";
		position[] = {1919,1787,0};

		teleporter = "staging_14_flagpole";

		motorpoolLocations[] = {
			"staging_14_motorpool_1",
			"staging_14_motorpool_2",
			"staging_14_motorpool_3",
			"staging_14_motorpool_4"
		};
		motorpoolLabels[] = {
			"Vilar Parking 1",
			"Vilar Parking 2",
			"Vilar Parking 3",
			"Vilar Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_14_resupply_1",
			"staging_14_resupply_2",
			"staging_14_resupply_3",
			"staging_14_resupply_4"
		};
	};
};