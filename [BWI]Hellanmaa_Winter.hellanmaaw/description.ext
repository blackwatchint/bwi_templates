author = "Black Watch International";
OnLoadName = "Hellanmaa Winter";
OnLoadMission = "Build: 07NOV2021";
enableDebugConsole = 0;

/**
 * CfgStagingAreas
 *   teleporters[] = Spawn teleporters (west, east, indep). If only 1 specified it applies for all.
 *
 * Staging
 *   name = Name of the staging area.
 *   position = Position of marker in 3DEN.
 *   teleporter = Staging area game logic object.
 *
 *   motorpoolLocations[] = Game logic objects denoting motorpool spawn locations.
 *   motorpoolLabels[] = User-friendly labels to match with the above locations.
 *   motorpoolTypes[] = Types of vehicles allowed at the above locations, as follows:
 *				1) Ground (Vehicles)
 *				2) Helipad (Helicopters)
 *				3) Hangar (Jet CAS)
 *				4) Apron (Air Transport)
 *				5) Carrier (Carrier Jet CAS)
 *			 	6) Sea (Boats)
 * 
 * 	resupplyLocations[] = Game logic objects denoting resupply spawn locations.
 */
class CfgStagingAreas
{
	teleporters[] = {"spawn_flagpole"};

	class Staging_1
	{
		name = "Hoopakka Communications";
		position[] = {4518.66,3243.14,0};

		teleporter = "staging_1_flagpole";

		motorpoolLocations[] = {
			"staging_1_motorpool_1", 
			"staging_1_motorpool_2"
		};
		motorpoolLabels[] = {
			"Comms Yard 1", 
			"Comms Yard 2"
		};
		motorpoolTypes[] = {1,1};

		resupplyLocations[] = {
			"staging_1_resupply_1",
			"staging_1_resupply_2",
			"staging_1_resupply_3"
		};
	};

	class Staging_2
	{
		name = "Nyman Warehouse Ruins";
		position[] = {1630.96,875.097,0};

		teleporter = "staging_2_flagpole";

		motorpoolLocations[] = {
			"staging_2_motorpool_1",
			"staging_2_motorpool_2"
		};
		motorpoolLabels[] = {
			"Warehouse Yard 1",
			"Warehouse Yard 2"
		};
		motorpoolTypes[] = {1,1};

		resupplyLocations[] = {
			"staging_2_resupply_1",
			"staging_2_resupply_2",
			"staging_2_resupply_3",
			"staging_2_resupply_4"
		};
	};

	class Staging_3
	{
		name = "Troihari Refinery";
		position[] = {257.588,3438.95,0};

		teleporter = "staging_3_flagpole";

		motorpoolLocations[] = {
			"staging_3_motorpool_1",
			"staging_3_motorpool_2",
			"staging_3_motorpool_3",
			"staging_3_motorpool_4"
		};
		motorpoolLabels[] = {
			"Refinery Garage 1",
			"Refinery Garage 2",
			"Refinery Warehouse 1",
			"Refinery Warehouse 2"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_3_resupply_1",
			"staging_3_resupply_2",
			"staging_3_resupply_3",
			"staging_3_resupply_4"
		};
	};

	class Staging_4
	{
		name = "Somppi Storage";
		position[] = {3763.71,5967,0};

		teleporter = "staging_4_flagpole";

		motorpoolLocations[] = {
			"staging_4_motorpool_1",
			"staging_4_motorpool_2"
		};
		motorpoolLabels[] = {
			"Storage Warehouse 1 ",
			"Storage Warehouse 2"
		};
		motorpoolTypes[] = {1,1};

		resupplyLocations[] = {
			"staging_4_resupply_1",
			"staging_4_resupply_2",
			"staging_4_resupply_3",
			"staging_4_resupply_4"
		};
	};

	class Staging_5
	{
		name = "Jussila Supermarket";
		position[] = {7452.32,6761.5,0};

		teleporter = "staging_5_flagpole";

		motorpoolLocations[] = {
			"staging_5_motorpool_1",
			"staging_5_motorpool_2",
			"staging_5_motorpool_3",
			"staging_5_motorpool_4"
		};
		motorpoolLabels[] = {
			"Supermarket Parking 1",
			"Supermarket Parking 2",
			"Supermarket Parking 3",
			"Supermarket Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_5_resupply_1",
			"staging_5_resupply_2",
			"staging_5_resupply_3",
			"staging_5_resupply_4"
		};
	};

	class Staging_6
	{
		name = "Rautakorpi Containers";
		position[] = {1085.93,6759.24,0};

		teleporter = "staging_6_flagpole";

		motorpoolLocations[] = {
			"staging_6_motorpool_1",
			"staging_6_motorpool_2",
			"staging_6_motorpool_3",
		};
		motorpoolLabels[] = {
			"Villa Courtyard 1",
			"Villa Courtyard 2",
			"Villa Courtyard 3",
		};
		motorpoolTypes[] = {1,1,1};

		resupplyLocations[] = {
			"staging_6_resupply_1",
			"staging_6_resupply_2",
			"staging_6_resupply_3",
			"staging_6_resupply_4"
		};
	};

	class Staging_7
	{
		name = "Kettula Timberyard";
		position[] = {5622.98,817.671,0};

		teleporter = "staging_7_flagpole";

		motorpoolLocations[] = {
			"staging_7_motorpool_1",
			"staging_7_motorpool_2"
		};
		motorpoolLabels[] = {
			"Timberyard Parking 1",
			"Timberyard Parking 2"
		};
		motorpoolTypes[] = {1,1};

		resupplyLocations[] = {
			"staging_7_resupply_1",
			"staging_7_resupply_2",
			"staging_7_resupply_3",
			"staging_7_resupply_4"
		};
	};

	class Staging_8
	{
		name = "Kontio Barns";
		position[] = {7578.3,3278.75,0};

		teleporter = "staging_8_flagpole";

		motorpoolLocations[] = {
			"staging_8_motorpool_1",
			"staging_8_motorpool_2",
			"staging_8_motorpool_3",
			"staging_8_motorpool_4"
		};
		motorpoolLabels[] = {
			"Barn Parking 1",
			"Barn Parking 2",
			"Barn Parking 3",
			"Barn Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,2};

		resupplyLocations[] = {
			"staging_8_resupply_1",
			"staging_8_resupply_2",
			"staging_8_resupply_3",
			"staging_8_resupply_4"
		};
	};
};