author = "Black Watch International";
OnLoadName = "Ruha";
OnLoadMission = "Build: 07NOV2021";
enableDebugConsole = 0;

/**
 * CfgStagingAreas
 *   teleporters[] = Spawn teleporters (west, east, indep). If only 1 specified it applies for all.
 *
 * Staging
 *   name = Name of the staging area.
 *   position = Position of marker in 3DEN.
 *   teleporter = Staging area game logic object.
 *
 *   motorpoolLocations[] = Game logic objects denoting motorpool spawn locations.
 *   motorpoolLabels[] = User-friendly labels to match with the above locations.
 *   motorpoolTypes[] = Types of vehicles allowed at the above locations, as follows:
 *				1) Ground (Vehicles)
 *				2) Helipad (Helicopters)
 *				3) Hangar (Jet CAS)
 *				4) Apron (Air Transport)
 *				5) Carrier (Carrier Jet CAS)
 *			 	6) Sea (Boats)
 * 
 * 	resupplyLocations[] = Game logic objects denoting resupply spawn locations.
 */
class CfgStagingAreas
{
	teleporters[] = {"spawn_flagpole"};

	class Staging_1
	{
		name = "Metsala Power Station";
		position[] = {6627.57,1580.42,0};

		teleporter = "staging_1_flagpole";

		motorpoolLocations[] = {
			"staging_1_motorpool_1", 
			"staging_1_motorpool_2", 
			"staging_1_motorpool_3", 
			"staging_1_motorpool_4"
		};
		motorpoolLabels[] = {
			"Power Station Yard 1", 
			"Power Station Yard 2", 
			"Power Station Yard 3",
			"Power Station Yard 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_1_resupply_1",
			"staging_1_resupply_2",
			"staging_1_resupply_3",
			"staging_1_resupply_4"
		};
	};

	class Staging_2
	{
		name = "Metsala Airfield";
		position[] = {6685.01,1161.44,0};

		teleporter = "staging_2_flagpole";

		motorpoolLocations[] = {
			"staging_2_motorpool_1",
			"staging_2_motorpool_2",
			"staging_2_motorpool_3",
			"staging_2_motorpool_4",
			"staging_2_motorpool_5",
			"staging_2_motorpool_6",
			"staging_2_motorpool_7"
		};
		motorpoolLabels[] = {
			"Airfield Parking 1",
			"Airfield Parking 2",
			"Airfield Parking 3",
			"Airfield Helipad 1",
			"Airfield Helipad 2",
			"Airfield Hangar 1",
			"Airfield Apron 1"
		};
		motorpoolTypes[] = {1,1,1,2,2,3,4};

		resupplyLocations[] = {
			"staging_2_resupply_1",
			"staging_2_resupply_2",
			"staging_2_resupply_3",
			"staging_2_resupply_4"
		};
	};

	class Staging_3
	{
		name = "Varjo Hotel";
		position[] = {3584.04,279.881,0};

		teleporter = "staging_3_flagpole";

		motorpoolLocations[] = {
			"staging_3_motorpool_1",
			"staging_3_motorpool_2",
			"staging_3_motorpool_3",
			"staging_3_motorpool_4"
		};
		motorpoolLabels[] = {
			"Hotel Parking 1",
			"Hotel Parking 2",
			"Hotel Parking 3",
			"Hotel Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_3_resupply_1",
			"staging_3_resupply_2",
			"staging_3_resupply_3"
		};
	};

	class Staging_4
	{
		name = "Martik Garages";
		position[] = {1399.55,106.489,0};

		teleporter = "staging_4_flagpole";

		motorpoolLocations[] = {
			"staging_4_motorpool_1",
			"staging_4_motorpool_2",
			"staging_4_motorpool_3",
			"staging_4_motorpool_4",
			"staging_4_motorpool_5"
		};
		motorpoolLabels[] = {
			"Martik Garage 1",
			"Martik Garage 2",
			"Martik Garage 3",
			"Martik Garage 4",
			"Martik Garage 5"
		};
		motorpoolTypes[] = {1,1,1,1,1};

		resupplyLocations[] = {
			"staging_4_resupply_1",
			"staging_4_resupply_2",
			"staging_4_resupply_3",
			"staging_4_resupply_4"
		};
	};

	class Staging_5
	{
		name = "Virpimaki Logisitcs";
		position[] = {5150.32,2190.54,0};

		teleporter = "staging_5_flagpole";

		motorpoolLocations[] = {
			"staging_5_motorpool_1",
			"staging_5_motorpool_2",
			"staging_5_motorpool_3",
			"staging_5_motorpool_4"
		};
		motorpoolLabels[] = {
			"Logisitcs Yard 1",
			"Logisitcs Yard 2",
			"Logisitcs Yard 3",
			"Logisitcs Yard 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_5_resupply_1",
			"staging_5_resupply_2",
			"staging_5_resupply_3",
			"staging_5_resupply_4"
		};
	};

	class Staging_6
	{
		name = "Kaaranmannikko Stadium";
		position[] = {6119.92,6973.71,0};

		teleporter = "staging_6_flagpole";

		motorpoolLocations[] = {
			"staging_6_motorpool_1",
			"staging_6_motorpool_2",
			"staging_6_motorpool_3",
			"staging_6_motorpool_4"
		};
		motorpoolLabels[] = {
			"Stadium Entrance 1",
			"Stadium Entrance 2",
			"Stadium Helipad 3",
			"Stadium Helipad 4"
		};
		motorpoolTypes[] = {1,1,2,2};

		resupplyLocations[] = {
			"staging_6_resupply_1",
			"staging_6_resupply_2",
			"staging_6_resupply_3",
			"staging_6_resupply_4"
		};
	};

	class Staging_7
	{
		name = "Kivimaki Racetrack";
		position[] = {7694.47,6536.81,0};

		teleporter = "staging_7_flagpole";

		motorpoolLocations[] = {
			"staging_7_motorpool_1",
			"staging_7_motorpool_2",
			"staging_7_motorpool_3",
			"staging_7_motorpool_4"
		};
		motorpoolLabels[] = {
			"Racetrack Garage 1",
			"Racetrack Garage 2",
			"Racetrack Garage 3",
			"Racetrack Garage 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_7_resupply_1",
			"staging_7_resupply_2",
			"staging_7_resupply_3",
			"staging_7_resupply_4"
		};
	};

	class Staging_8
	{
		name = "Pihlajamaa Farm";
		position[] = {2270.3,7008.02,0};

		teleporter = "staging_8_flagpole";

		motorpoolLocations[] = {
			"staging_8_motorpool_1",
			"staging_8_motorpool_2",
			"staging_8_motorpool_3",
			"staging_8_motorpool_4"
		};
		motorpoolLabels[] = {
			"Farm House 1",
			"Farm House 2",
			"Farm Field 1",
			"Farm Field 2"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_8_resupply_1",
			"staging_8_resupply_2",
			"staging_8_resupply_3",
			"staging_8_resupply_4"
		};
	};

	class Staging_9
	{
		name = "Ruha Construction Yard";
		position[] = {3449.39,4043.57,0};

		teleporter = "staging_9_flagpole";

		motorpoolLocations[] = {
			"staging_9_motorpool_1",
			"staging_9_motorpool_2",
			"staging_9_motorpool_3"
		};
		motorpoolLabels[] = {
			"Construction Yard 1",
			"Construction Yard 2",
			"Construction Warehouse 1",
		};
		motorpoolTypes[] = {1,1,1};

		resupplyLocations[] = {
			"staging_9_resupply_1",
			"staging_9_resupply_2",
			"staging_9_resupply_3"
		};
	};
};