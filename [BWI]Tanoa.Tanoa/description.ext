author = "Black Watch International";
OnLoadName = "Tanoa";
OnLoadMission = "Build: 07NOV2021";
enableDebugConsole = 0;

/**
 * CfgStagingAreas
 *   teleporters[] = Spawn teleporters (west, east, indep). If only 1 specified it applies for all.
 *
 * Staging
 *   name = Name of the staging area.
 *   position = Position of marker in 3DEN.
 *   teleporter = Staging area game logic object.
 *
 *   motorpoolLocations[] = Game logic objects denoting motorpool spawn locations.
 *   motorpoolLabels[] = User-friendly labels to match with the above locations.
 *   motorpoolTypes[] = Types of vehicles allowed at the above locations, as follows:
 *				1) Ground (Vehicles)
 *				2) Helipad (Helicopters)
 *				3) Hangar (Jet CAS)
 *				4) Apron (Air Transport)
 *				5) Carrier (Carrier Jet CAS)
 *			 	6) Sea (Boats)
 * 
 * 	resupplyLocations[] = Game logic objects denoting resupply spawn locations.
 */
class CfgStagingAreas
{
	teleporters[] = {"spawn_flagpole"};

	class Staging_1
	{
		name = "USS Freedom";
		position[] = {7177.51,700.556,74.6662};

		teleporter = "staging_1_flagpole";

		motorpoolLocations[] = {
			"staging_1_motorpool_1", 
			"staging_1_motorpool_2", 
			"staging_1_motorpool_3", 
			"staging_1_motorpool_4"
		};
		motorpoolLabels[] = {
			"Freedom Deck 1", 
			"Starboard Elevator 1",
			"Starboard Elevator 2",
			"Port Elevator 1",
		};
		motorpoolTypes[] = {1,5,5,2};

		resupplyLocations[] = {
			"staging_1_resupply_1",
			"staging_1_resupply_2",
			"staging_1_resupply_3"
		};
	};

	class Staging_2
	{
		name = "USS Liberty";
		position[] = {6498.9,412.661,67.0189};

		teleporter = "staging_2_flagpole";

		motorpoolLocations[] = {
			"staging_2_motorpool_1",
			"staging_2_motorpool_2",
			"staging_2_motorpool_3",
			"staging_2_motorpool_4",
			"staging_2_motorpool_5"
		};
		motorpoolLabels[] = {
			"Liberty Helipad 1",
			"Starboard Ladder 1",
			"Starboard Ladder 2",
			"Port Ladder 1",
			"Port Ladder 2"
		};
		motorpoolTypes[] = {2,6,6,6,6};

		resupplyLocations[] = {
			"staging_2_resupply_1",
			"staging_2_resupply_2",
			"staging_2_resupply_3",
			"staging_2_resupply_4"
		};
	};

	class Staging_3
	{
		name = "Aéroport de Tanoa Terminal";
		position[] = {6912,7426,0};

		teleporter = "staging_3_flagpole";

		motorpoolLocations[] = {
			"staging_3_motorpool_1",
			"staging_3_motorpool_2",
			"staging_3_motorpool_3",
			"staging_3_motorpool_4",
			"staging_3_motorpool_5"
		};
		motorpoolLabels[] = {
			"Aéroport Garage 1",
			"Aéroport Garage 2",
			"Aéroport Garage 3",
			"Aéroport Garage 4",
			"Aéroport Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_3_resupply_1",
			"staging_3_resupply_2",
			"staging_3_resupply_3",
			"staging_3_resupply_4"
		};
	};

	class Staging_4
	{
		name = "Aéroport de Tanoa Hangars";
		position[] = {6900.32,7249.22,0};

		teleporter = "staging_4_flagpole";

		motorpoolLocations[] = {
			"staging_4_motorpool_1",
			"staging_4_motorpool_2",
			"staging_4_motorpool_3"
		};
		motorpoolLabels[] = {
			"Aéroport Hangar 1",
			"Aéroport Hangar 2",
			"Aéroport Large Hangar 1"
		};
		motorpoolTypes[] = {3,3,4};

		resupplyLocations[] = {
			"staging_4_resupply_1",
			"staging_4_resupply_2",
			"staging_4_resupply_3",
			"staging_4_resupply_4"
		};
	};

	class Staging_5
	{
		name = "Rochelle AFB Terminal";
		position[] = {11656.3,13118.5,0};

		teleporter = "staging_5_flagpole";

		motorpoolLocations[] = {
			"staging_5_motorpool_1",
			"staging_5_motorpool_2",
			"staging_5_motorpool_3",
			"staging_5_motorpool_4"
		};
		motorpoolLabels[] = {
			"Rochelle Parking 1",
			"Rochelle Parking 2",
			"Rochelle Parking 3",
			"Rochelle Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_5_resupply_1",
			"staging_5_resupply_2",
			"staging_5_resupply_3",
			"staging_5_resupply_4"
		};
	};

	class Staging_6
	{
		name = "Rochelle AFB Hangars";
		position[] = {11808.3,13054.9,0};

		teleporter = "staging_6_flagpole";

		motorpoolLocations[] = {
			"staging_6_motorpool_1",
			"staging_6_motorpool_2",
			"staging_6_motorpool_3"
		};
		motorpoolLabels[] = {
			"Rochelle Hangar 1",
			"Rochelle Hangar 2",
			"Rochelle Apron 1"
		};
		motorpoolTypes[] = {3,3,4};

		resupplyLocations[] = {
			"staging_6_resupply_1",
			"staging_6_resupply_2",
			"staging_6_resupply_3",
			"staging_6_resupply_4"
		};
	};

	class Staging_7
	{
		name = "Comms Bravo";
		position[] = {11146,11441,0};

		teleporter = "staging_7_flagpole";

		motorpoolLocations[] = {
			"staging_7_motorpool_1",
			"staging_7_motorpool_2",
			"staging_7_motorpool_3",
			"staging_7_motorpool_4",
			"staging_7_motorpool_5",
			"staging_7_motorpool_6",
			"staging_7_motorpool_7"
		};
		motorpoolLabels[] = {
			"Comms Bravo Parking 1",
			"Comms Bravo Parking 2",
			"Comms Bravo Parking 3",
			"Comms Bravo Parking 4",
			"Comms Bravo Helipad 1",
			"Comms Bravo Helipad 2",
			"Comms Bravo Helipad 3"
		};
		motorpoolTypes[] = {1,1,1,1,2,2,2};

		resupplyLocations[] = {
			"staging_7_resupply_1",
			"staging_7_resupply_2",
			"staging_7_resupply_3",
			"staging_7_resupply_4"
		};
	};

	class Staging_8
	{
		name = "Nicolet Harbor";
		position[] = {6379.27,12886.8,0};

		teleporter = "staging_8_flagpole";

		motorpoolLocations[] = {
			"staging_8_motorpool_1",
			"staging_8_motorpool_2",
			"staging_8_motorpool_3",
			"staging_8_motorpool_4",
			"staging_8_motorpool_5",
			"staging_8_motorpool_6"
		};
		motorpoolLabels[] = {
			"Nicolet Parking 1",
			"Nicolet Parking 2",
			"Nicolet Parking 3",
			"Nicolet Parking 4",
			"Nicolet Mooring 1",
			"Nicolet Mooring 2"
		};
		motorpoolTypes[] = {1,1,1,1,6,6};

		resupplyLocations[] = {
			"staging_8_resupply_1",
			"staging_8_resupply_2",
			"staging_8_resupply_3",
			"staging_8_resupply_4"
		};
	};
	
	class Staging_9
	{
		name = "Leqa Ferry";
		position[] = {2249,8586,0};

		teleporter = "staging_9_flagpole";

		motorpoolLocations[] = {
			"staging_9_motorpool_1",
			"staging_9_motorpool_2",
			"staging_9_motorpool_3",
			"staging_9_motorpool_4",
			"staging_9_motorpool_5",
			"staging_9_motorpool_6",
			"staging_9_motorpool_7",
			"staging_9_motorpool_8",
		};
		motorpoolLabels[] = {
			"Leqa Parking 1",
			"Leqa Parking 2",
			"Leqa Parking 3",
			"Leqa Parking 4",
			"Leqa Pier 1",
			"Leqa Pier 2",
			"Leqa Pier 3",
			"Leqa Pier 4",
		};
		motorpoolTypes[] = {1,1,1,1,6,6,6,6};

		resupplyLocations[] = {
			"staging_9_resupply_1",
			"staging_9_resupply_2",
			"staging_9_resupply_3",
			"staging_9_resupply_4"
		};
	};
	
	class Staging_10
	{
		name = "Yanukka Church";
		position[] = {3197.85,3383.84,0};

		teleporter = "staging_10_flagpole";

		motorpoolLocations[] = {
			"staging_10_motorpool_1",
			"staging_10_motorpool_2",
			"staging_10_motorpool_3",
			"staging_10_motorpool_4"
		};
		motorpoolLabels[] = {
			"Yanukka Parking 1",
			"Yanukka Parking 2",
			"Yanukka Parking 3",
			"Yanukka Parking 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_10_resupply_1",
			"staging_10_resupply_2",
			"staging_10_resupply_3",
			"staging_10_resupply_4"
		};
	};
	
	class Staging_11
	{
		name = "Ouméré Gas Station";
		position[] = {12638,7597.89,0};

		teleporter = "staging_11_flagpole";

		motorpoolLocations[] = {
			"staging_11_motorpool_1",
			"staging_11_motorpool_2",
			"staging_11_motorpool_3",
			"staging_11_motorpool_4"
		};
		motorpoolLabels[] = {
			"Ouméré Parking 1",
			"Ouméré Parking 2",
			"Ouméré Parking 3",
			"Ouméré Parking 4",
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_11_resupply_1",
			"staging_11_resupply_2",
			"staging_11_resupply_3",
			"staging_11_resupply_4"
		};
	};
	
	class Staging_12
	{
		name = "Tuvanaka AFB Barracks";
		position[] = {2370.72,13307.2,0};
		
		teleporter = "staging_12_flagpole";

		motorpoolLocations[] = {
			"staging_12_motorpool_1",
			"staging_12_motorpool_2",
			"staging_12_motorpool_3",
			"staging_12_motorpool_4",
			"staging_12_motorpool_5"
		};
		motorpoolLabels[] = {
			"Tuvanaka Parking 1",
			"Tuvanaka Parking 2",
			"Tuvanaka Parking 3",
			"Tuvanaka Parking 4",
			"Tuvanaka Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_12_resupply_1",
			"staging_12_resupply_2",
			"staging_12_resupply_3",
			"staging_12_resupply_4"
		};
	};
	
	class Staging_13
	{
		name = "Tuvanaka AFB Hangars";
		position[] = {2202.52,13415.1,0};
		
		teleporter = "staging_13_flagpole";

		motorpoolLocations[] = {
			"staging_13_motorpool_1",
			"staging_13_motorpool_2",
			"staging_13_motorpool_3"
		};
		motorpoolLabels[] = {
			"Tuvanaka Hangar 1",
			"Tuvanaka Hangar 2",
			"Tuvanaka Apron 1"
		};
		motorpoolTypes[] = {3,3,4};

		resupplyLocations[] = {
			"staging_13_resupply_1",
			"staging_13_resupply_2",
			"staging_13_resupply_3",
			"staging_13_resupply_4"
		};
	};
	
	class Staging_14
	{
		name = "Saint-George Gas Station";
		position[] = {11602.1,2995.9,0};
		
		teleporter = "staging_14_flagpole";

		motorpoolLocations[] = {
			"staging_14_motorpool_1",
			"staging_14_motorpool_2",
			"staging_14_motorpool_3",
			"staging_14_motorpool_4"
		};
		motorpoolLabels[] = {
			"Saint-George Parking 1",
			"Saint-George Parking 2",
			"Saint-George Parking 3",
			"Saint-George Parking 4",
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_14_resupply_1",
			"staging_14_resupply_2",
			"staging_14_resupply_3",
			"staging_14_resupply_4"
		};
	};
	
	class Staging_15
	{
		name = "Saint-George AFB Hangars";
		position[] = {11745.1,3145.59,0};
		
		teleporter = "staging_15_flagpole";

		motorpoolLocations[] = {
			"staging_15_motorpool_1",
			"staging_15_motorpool_2",
			"staging_15_motorpool_3",
			"staging_15_motorpool_4"
		};
		motorpoolLabels[] = {
			"Saint-George Helipad",
			"Saint-George Hangar 1",
			"Saint-George Hangar 2",
			"Saint-George Apron 1"
		};
		motorpoolTypes[] = {2,3,3,4};

		resupplyLocations[] = {
			"staging_15_resupply_1",
			"staging_15_resupply_2",
			"staging_15_resupply_3",
			"staging_15_resupply_4"
		};
	};
};