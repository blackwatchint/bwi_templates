author = "Black Watch International";
OnLoadName = "Zargabad";
OnLoadMission = "Build: 07NOV2021";
enableDebugConsole = 0

/**
 * CfgStagingAreas
 *   teleporters[] = Spawn teleporters (west, east, indep). If only 1 specified it applies for all.
 *
 * Staging
 *   name = Name of the staging area.
 *   position = Position of marker in 3DEN.
 *   teleporter = Staging area game logic object.
 *
 *   motorpoolLocations[] = Game logic objects denoting motorpool spawn locations.
 *   motorpoolLabels[] = User-friendly labels to match with the above locations.
 *   motorpoolTypes[] = Types of vehicles allowed at the above locations, as follows:
 *				1) Ground (Vehicles)
 *				2) Helipad (Helicopters)
 *				3) Hangar (Jet CAS)
 *				4) Apron (Air Transport)
 *				5) Carrier (Carrier Jet CAS)
 *			 	6) Sea (Boats)
 * 
 * 	resupplyLocations[] = Game logic objects denoting resupply spawn locations.
 */
class CfgStagingAreas
{
	teleporters[] = {"spawn_flagpole"};

	class Staging_1
	{
		name = "Zargabad AFB Barracks";
		position[] = {3572.38,4038.35,0};

		teleporter = "staging_1_flagpole";

		motorpoolLocations[] = {
			"staging_1_motorpool_1", 
			"staging_1_motorpool_2",
			"staging_1_motorpool_3",
			"staging_1_motorpool_4",
			"staging_1_motorpool_5",
			"staging_1_motorpool_6"
		};
		motorpoolLabels[] = {
			"Zargabad Parking 1", 
			"Zargabad Parking 2", 
			"Zargabad Parking 3",
			"Zargabad Parking 4",
			"Zargabad Helipad 1",
			"Zargabad Helipad 2"

		};
		motorpoolTypes[] = {1,1,1,1,2,2};

		resupplyLocations[] = {
			"staging_1_resupply_1",
			"staging_1_resupply_2",
			"staging_1_resupply_3",
			"staging_1_resupply_4"
		};
	};

	class Staging_2
	{
		name = "Zargabad AFB Hangars";
		position[] = {3544.45,4147.61,0};

		teleporter = "staging_2_flagpole";

		motorpoolLocations[] = {
			"staging_2_motorpool_1", 
			"staging_2_motorpool_2"
		};
		motorpoolLabels[] = {
			"Zargabad Hangar 1",
			"Zargabad Hangar 2"

		};
		motorpoolTypes[] = {3,3};

		resupplyLocations[] = {
		};
	};

	class Staging_3
	{
		name = "FOB Ab-e Shur";
		position[] = {3973.78,2761.89,0};

		teleporter = "staging_3_flagpole";

		motorpoolLocations[] = {
			"staging_3_motorpool_1",
			"staging_3_motorpool_2",
			"staging_3_motorpool_3",
			"staging_3_motorpool_4"
			
		};
		motorpoolLabels[] = {
			"Ab-e Shur Parking 1",
			"Ab-e Shur Parking 2",
			"Ab-e Shur Parking 3",
			"Ab-e Shur Parking 4"			
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_3_resupply_1",
			"staging_3_resupply_2",
			"staging_3_resupply_3",
			"staging_3_resupply_4"
		};
	};

	class Staging_4
	{
		name = "The Villa";
		position[] = {4862.69,4613.9,0};

		teleporter = "staging_4_flagpole";

		motorpoolLocations[] = {
			"staging_4_motorpool_1",
			"staging_4_motorpool_2",
			"staging_4_motorpool_3",
			"staging_4_motorpool_4",
			"staging_4_motorpool_5"
		};
		motorpoolLabels[] = {
			"Villa Roadside 1",
			"Villa Roadside 2",
			"Villa Roadside 3",
			"Villa Roadside 4",
			"Villa Helipad 1"		
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_4_resupply_1",
			"staging_4_resupply_2",
			"staging_4_resupply_3",
			"staging_4_resupply_4"
		};
	};

	class Staging_5
	{
		name = "FOB Hazar Bagh";
		position[] = {4972.06,6145.29,0};

		teleporter = "staging_5_flagpole";

		motorpoolLocations[] = {
			"staging_5_motorpool_1",
			"staging_5_motorpool_2",
			"staging_5_motorpool_3",
			"staging_5_motorpool_4",
			"staging_5_motorpool_5",
			"staging_5_motorpool_6",
		};
		motorpoolLabels[] = {
			"Hazar Bagh Parking 1",
			"Hazar Bagh Parking 2",
			"Hazar Bagh Parking 3",
			"Hazar Bagh Parking 4",
			"Hazar Bagh Helipad 1",
			"Hazar Bagh Helipad 2"		
		};
		motorpoolTypes[] = {1,1,1,1,2,2};

		resupplyLocations[] = {
			"staging_5_resupply_1",
			"staging_5_resupply_2",
			"staging_5_resupply_3",
			"staging_5_resupply_4"
		};
	};

	class staging_6
	{
		name = "Firuz Baharv";
		position[] = {5018.44,1901.07,0};

		teleporter = "staging_6_flagpole";

		motorpoolLocations[] = {
			"staging_6_motorpool_1",
			"staging_6_motorpool_2",
			"staging_6_motorpool_3",
			"staging_6_motorpool_4"
		};
		motorpoolLabels[] = {
			"Firuz Baharv Parking 1",
			"Firuz Baharv Parking 2",
			"Firuz Baharv Parking 3",
			"Firuz Baharv Parking 4"		
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_6_resupply_1",
			"staging_6_resupply_2",
			"staging_6_resupply_3",
			"staging_6_resupply_4"
		};
	};
	
	class Staging_7
	{
		name = "FOB Azizayt";
		position[] = {1876.6,4675.02,0};

		teleporter = "staging_7_flagpole";

		motorpoolLocations[] = {
			"staging_7_motorpool_1",
			"staging_7_motorpool_2",
			"staging_7_motorpool_3",
			"staging_7_motorpool_4",
			"staging_7_motorpool_5"
		};
		motorpoolLabels[] = {
			"Azizayt Parking 1",
			"Azizayt Parking 2",
			"Azizayt Parking 3",
			"Azizayt Parking 4",
			"Azizayt Helipad 1"
		};
		motorpoolTypes[] = {1,1,1,1,2};

		resupplyLocations[] = {
			"staging_7_resupply_1",
			"staging_7_resupply_2",
			"staging_7_resupply_3",
			"staging_7_resupply_4"
		};
	};
	
	class Staging_8
	{
		name = "Zargabad Mosque";
		position[] = {4258.59,4155.07,0};

		teleporter = "staging_7_flagpole";

		motorpoolLocations[] = {
			"staging_8_motorpool_1",
			"staging_8_motorpool_2",
			"staging_8_motorpool_3",
			"staging_8_motorpool_4"
		};
		motorpoolLabels[] = {
			"Mosque Roadside 1",
			"Mosque Roadside 2",
			"Mosque Roadside 3",
			"Mosque Roadside 4"
		};
		motorpoolTypes[] = {1,1,1,1};

		resupplyLocations[] = {
			"staging_8_resupply_1",
			"staging_8_resupply_2",
			"staging_8_resupply_3",
			"staging_8_resupply_4"
		};
	};
};